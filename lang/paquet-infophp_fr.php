<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'infophp_description' => 'Ce plugin permet d\'afficher les infos PHP en restant dans l\'interface d\'admin de SPIP',
	'infophp_nom' => 'Info PHP',
	'infophp_slogan' => 'Afficher les info PHP de votre serveur'
	
);

?>
